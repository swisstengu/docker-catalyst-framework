#!/bin/bash

if [ -x /webapp/script/clean-cache ]; then
  echo 'Configure Perl'
  eval $(perl -I$HOME/perl5/lib/perl5 -Mlocal::lib)
  cd /webapp/script
  ./clean-cache
fi
