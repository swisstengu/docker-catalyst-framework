#!/bin/bash
FLAG=$(basename $APP_SOCKET .sock)

if [ -f ${HOME}/perl5/${FLAG} ]; then
  echo 'Already deployed'
else
  echo 'Configure Perl'
  eval $(perl -I${HOME}/perl5/lib/perl5 -Mlocal::lib)
  echo 'Configure CPAN'
  mkdir -p ~/.config/share/.cpan/CPAN
  mv /entrypoint.d/MyConfig.pm ~/.config/share/.cpan/CPAN
  echo 'Building libraries'
  cd /webapp
  echo '  Create Makefile'
  yes y | perl Makefile.PL
  echo '  Make'
  yes y | make
  touch ${HOME}/perl5/${FLAG}
fi
