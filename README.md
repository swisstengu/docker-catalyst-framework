# Catalyst-framework
## Catalyst-framework: a Perl MCV framework
More information: http://www.catalystframework.org/

## Purpose
This container installs several libs, probably many of them you won't need.
It's the base container for all my apps, but I thought it might help other people.

## Usage
Please mount your catalyst app in /webapp. That's all.

## Extend
You can drop files in entrypoint.d in order to extend the container - note that
it by defaults calls "perl Makefile.pl" and "make" in order to install all deps.

## Private repository?
Yes, because, for now, there are some stuff in there I can't fully open ;). Needs some cleanup and re-work
on my side. But I need this image, sooo…
