FROM debian:testing
MAINTAINER swisstengu <tengu@tengu.ch>

RUN apt update && apt-get upgrade -y && apt-get autoremove -y

RUN apt install -y perl
RUN apt install -y libcatalyst-* libdbix-class-*
RUN apt install -y libmath-round-perl \
                   libdbd-mysql-perl \
                   libfile-cache-perl \
                   libnet-amazon-s3-perl \
                   libcrypt-cbc-perl \
                   libcrypt-openssl-rsa-perl \
                   liblocal-lib-perl \
                   libmodule-install-perl \
                   sendmail \
                   locales-all \
                   libxml-feed-perl \
                   libtext-unaccent-perl \
                   libberkeleydb-perl \
                   libimager-perl \
                   libtext-lorem-perl \
                   libdata-password-perl \
                   libmojolicious-perl \
                   libdatetime-format-duration-perl \
                   libdatetime-format-mysql-perl \
                   libdatetime-format-strptime-perl \
                   libcalendar-simple-perl \
                   libdate-simple-perl \
                   libcrypt-generatepassword-perl \
                   build-essential

RUN apt clean all

COPY entrypoint /entrypoint
COPY entrypoint.debug /entrypoint.debug
RUN chmod +x /entrypoint*
COPY entrypoint.d/* /entrypoint.d/
RUN chmod +x /entrypoint.d/*
RUN useradd -d /webapp --uid 1024 catalyst
USER catalyst
CMD ["/entrypoint"]
